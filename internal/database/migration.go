package database

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/javiles/go-api/internal/model"
)

func MigrateDB(db *gorm.DB) error {
	if result := db.AutoMigrate(&model.Dummy{}); result.Error != nil {
		return result.Error
	}
	return nil
}
