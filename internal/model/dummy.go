package model

import (
	"github.com/jinzhu/gorm"
)

type Dummy struct {
	gorm.Model
	DummyId uint
	DummyName string
	DummyDesc string
}
