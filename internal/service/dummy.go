package service

import (
	errors "gitlab.com/javiles/go-api/internal/error"
	"gitlab.com/javiles/go-api/internal/model"
	"gitlab.com/javiles/go-api/internal/storage"
	"gitlab.com/javiles/go-api/internal/transport/http/response"
)

type DummyService struct{
	DummyRepository *storage.DummyRepository
}

func NewDummyService(dummyRepository *storage.DummyRepository) *DummyService {
	return &DummyService{
		DummyRepository: dummyRepository,
	}
}


func (s *DummyService) GetDummyById(ID uint) response.Message {
	dummy, err := s.DummyRepository.GetDummyById(ID)
	if err != nil {
		return response.BuildErrorResponseMessage(errors.DummyGetItIsNotPossible, errors.DummyErrorMap)
	}
	return response.Message{CodError: 0, DescError: "OK", DTO: dummy}
}

func (s *DummyService) SaveDummy(dummy model.Dummy) response.Message {
	dummy, err := s.DummyRepository.SaveDummy(dummy)
	if err != nil {
		return response.BuildErrorResponseMessage(errors.DummySaveItIsNotPossible, errors.DummyErrorMap)
	}
	return response.Message{CodError: 0, DescError: "OK", DTO: dummy}
}

func (s *DummyService) UpdateDummy(Id uint, newDummy model.Dummy) response.Message {
	dummy, err := s.DummyRepository.UpdateDummy(Id,newDummy)
	if err != nil {
		return response.BuildErrorResponseMessage(errors.DummyUpdateItIsNotPossible, errors.DummyErrorMap)
	}
	return response.Message{CodError: 0, DescError: "OK", DTO: dummy}
}

func (s *DummyService) DeleteDummy(Id uint) response.Message {
	err := s.DummyRepository.DeleteDummy(Id)
	if err != nil {
		return response.BuildErrorResponseMessage(errors.DummyDeleteItIsNotPossible, errors.DummyErrorMap)
	}
	return response.Message{CodError: 0, DescError: "OK", DTO: nil}
}

func (s *DummyService) ListDummies() response.Message {
	dummies, err := s.DummyRepository.ListDummies()
	if err != nil {
		return response.BuildErrorResponseMessage(errors.DummyListItIsNotPossible, errors.DummyErrorMap)
	}
	return response.Message{CodError: 0, DescError: "OK", DTO: dummies}
}

