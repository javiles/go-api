package response

import (
	"encoding/json"
	"net/http"
)

const (
	Successful           int = 0
	Created              int = 1
	NotDataFound         int = 2
	PreconditionRequired int = 3
	GeneralError         int = 4

)

type Message struct{
	CodError int
	DescError string
	DTO interface{}
}

type Entity struct{

}


func BuildHttpResponse(w http.ResponseWriter, message Message) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	switch message.CodError {
		case Successful:
			w.WriteHeader(http.StatusOK)
		case Created:
			w.WriteHeader(http.StatusCreated)
		case NotDataFound:
			w.WriteHeader(http.StatusNotFound)
		case PreconditionRequired:
			w.WriteHeader(http.StatusPreconditionRequired)
		case GeneralError:
			w.WriteHeader(http.StatusInternalServerError)
		default:
			w.WriteHeader(http.StatusPreconditionRequired)
	}

	if err := json.NewEncoder(w).Encode(message); err !=nil {
		panic(err)
	}

}

func BuildErrorResponseMessage(errorCode int, errorMap map[int]string) Message {
	return Message{CodError: errorCode, DescError: errorMap[errorCode], DTO: nil}
}