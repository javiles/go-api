package handler

import (
	"encoding/json"
	"fmt"
	errors "gitlab.com/javiles/go-api/internal/error"
	"gitlab.com/javiles/go-api/internal/service"
	"gitlab.com/javiles/go-api/internal/model"
	"gitlab.com/javiles/go-api/internal/transport/http/response"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type DummyHandler struct{
	Router       *mux.Router
	DummyService *service.DummyService
}

func NewDummyHandler(router *mux.Router,dummyService *service.DummyService) *DummyHandler {
	return &DummyHandler{
		Router:          router,
		DummyService: dummyService,
	}
}

func (h *DummyHandler) SetupRoutes(){
	fmt.Println("Setting Up Routes")

	h.Router.HandleFunc("/api/dummy/", h.GetDummies).Methods("GET")
	h.Router.HandleFunc("/api/dummy/", h.SaveDummy).Methods("POST")
	h.Router.HandleFunc("/api/dummy/{id}", h.GetDummy).Methods("GET")
	h.Router.HandleFunc("/api/dummy/{id}", h.UpdateDummy).Methods("PUT")
	h.Router.HandleFunc("/api/dummy/{id}", h.DeleteDummy).Methods("DELETE")

	h.Router.HandleFunc("/api/health", func(w http.ResponseWriter, r *http.Request) {
		response.BuildHttpResponse(w, response.Message{CodError: 0,	DescError: "OK", DTO: "Is alive"})
	})


}

func (h *DummyHandler) GetDummies (w http.ResponseWriter, r *http.Request) {
	response.BuildHttpResponse(w, h.DummyService.ListDummies())
}

func (h *DummyHandler) SaveDummy (w http.ResponseWriter, r *http.Request){
	var dummy model.Dummy
	if err:= json.NewDecoder(r.Body).Decode(&dummy); err != nil{
		response.BuildHttpResponse(w, response.BuildErrorResponseMessage(errors.JsonDecodeFailed, errors.GeneralErrorMap))
		return
	}

	response.BuildHttpResponse(w,  h.DummyService.SaveDummy(dummy))

}

func (h *DummyHandler) GetDummy(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	id := vars["id"]

	i , err := strconv.ParseUint(id,10, 64)
    if err != nil {
		response.BuildHttpResponse(w, response.BuildErrorResponseMessage(errors.UnableToParseId, errors.GeneralErrorMap))
		return
	}

	response.BuildHttpResponse(w,h.DummyService.GetDummyById(uint(i)))
}

func (h *DummyHandler) UpdateDummy(w http.ResponseWriter, r *http.Request){
    vars := mux.Vars(r)
	id := vars["id"]

	i , err := strconv.ParseUint(id,10, 64)
	if err != nil {
		response.BuildHttpResponse(w, response.BuildErrorResponseMessage(errors.UnableToParseId, errors.GeneralErrorMap))
		return
	}

	var dummy model.Dummy
	if err:= json.NewDecoder(r.Body).Decode(&dummy); err != nil{
		response.BuildHttpResponse(w, response.BuildErrorResponseMessage(errors.JsonDecodeFailed, errors.GeneralErrorMap))
		return
	}

	response.BuildHttpResponse(w, h.DummyService.UpdateDummy(uint(i), dummy))

}

func (h *DummyHandler) DeleteDummy(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	id := vars["id"]

	i , err := strconv.ParseUint(id,10, 64)
	if err != nil {
		response.BuildHttpResponse(w, response.BuildErrorResponseMessage(errors.UnableToParseId, errors.GeneralErrorMap))
		return
	}
	response.BuildHttpResponse(w, h.DummyService.DeleteDummy(uint(i)))
}
