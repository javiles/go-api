package storage

import (
	"github.com/jinzhu/gorm"
	model "gitlab.com/javiles/go-api/internal/model"
)

type DummyRepository struct {
	DB *gorm.DB
}


func NewDummyRepository(db *gorm.DB) *DummyRepository {
	return &DummyRepository{
		DB: db,
	}
}

func (s *DummyRepository) GetDummyById(ID uint) (model.Dummy, error) {
	var dummy model.Dummy
	if result := s.DB.First(&dummy, ID) ; result.Error != nil {
		return model.Dummy{}, result.Error
	}

	return dummy, nil
}

func (s *DummyRepository) SaveDummy(dummy model.Dummy) (model.Dummy, error) {
	if result := s.DB.Save(&dummy) ; result.Error != nil {
		return model.Dummy{}, result.Error
	}
	return 	dummy, nil
}

func (s *DummyRepository) UpdateDummy(Id uint, newDummy model.Dummy) (model.Dummy, error) {
	dummy , error := s.GetDummyById(Id)
	if error != nil {
		return model.Dummy{}, nil
	}

	if result := s.DB.Model(&dummy).Update(newDummy) ; result.Error != nil {
		return model.Dummy{}, result.Error
	}

	return dummy, nil
}

func (s *DummyRepository) DeleteDummy(Id uint) error {
	if result := s.DB.Delete(&model.Dummy{},Id) ; result.Error != nil {
		return result.Error
	}
	return nil
}

func (s *DummyRepository) ListDummies() ([]model.Dummy, error) {
	var dummies []model.Dummy

	if result := s.DB.Find(&dummies) ; result.Error != nil {
		return dummies, result.Error
	}
	return dummies, nil
}


