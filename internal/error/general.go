package error


const (
	JsonDecodeFailed        int = 10000
	UnableToParseId         int = 10001
)

var GeneralErrorMap = map[int]string {
	JsonDecodeFailed:     "Json Decode Failed",
	UnableToParseId:      "Unable to parse Id parameter",
}
