package error

const (
	DummyNotDataFound        int = 1000
	DummyAlreadyExists       int = 1001
	DummyGetItIsNotPossible  int = 1002
	DummySaveItIsNotPossible int = 1003
	DummyUpdateItIsNotPossible int = 1004
	DummyDeleteItIsNotPossible int = 1005
	DummyListItIsNotPossible int = 1006
)

var DummyErrorMap = map[int]string {
	DummyNotDataFound:       "Dummy not data found",
	DummyAlreadyExists:      "Dummy already exists",
	DummyGetItIsNotPossible: "Dummy It is not possible obtain",
	DummySaveItIsNotPossible: "Dummy It is not possible save",
	DummyUpdateItIsNotPossible: "Dummy It is not possible update",
	DummyDeleteItIsNotPossible: "Dummy It is not possible delete",
	DummyListItIsNotPossible: "Dummy It is not possible list",
}


