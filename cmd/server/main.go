package main

import (
  "fmt"
  "github.com/gorilla/mux"
  "github.com/jinzhu/gorm"
  "gitlab.com/javiles/go-api/internal/service"
  "gitlab.com/javiles/go-api/internal/storage"
  "gitlab.com/javiles/go-api/internal/database"
  "gitlab.com/javiles/go-api/internal/transport/http/handler"
  "net/http"
)

//App- struct para almacenar cosas como punteros a conexiones a base de datos
type App struct{
  router *mux.Router
  db *gorm.DB
}



// run - arranca la aplicacion
func (app *App) Run() error{
  fmt.Println("Setting Up Our App")

  db, err := database.NewDatabase()
  if err != nil {
    return err
  }

  err = database.MigrateDB(db)
  if err != nil {
    return err
  }

  app.db = db
  app.router = mux.NewRouter()

  dummyRepository := storage.NewDummyRepository(app.db)
  dummyService := service.NewDummyService(dummyRepository)
  dummyHandler := handler.NewDummyHandler(app.router,dummyService)
  dummyHandler.SetupRoutes()

  fmt.Println()

  if err := http.ListenAndServe(":8080", app.router); err != nil {
    fmt.Println("Failed to setup server")
  }

  return nil
}

func main() {
  fmt.Println("Vamos por esa api go")
  app := App{}
  if err := app.Run(); err != nil {
    fmt.Println("Error starting up our REST API")
    fmt.Println(err)
  }

}

